from sys import argv
import numpy as np
import cv2


def box_filter(src_path ='lena.jpg', dst_path ='output.jpg', size = 5):
    w, h = size, size
    if w % 2 == 0:
        w += 1
    if h % 2 == 0:
        h += 1

    img_orig = cv2.imread(src_path)
    width, height = img_orig.shape[1], img_orig.shape[0]
    img_sum = np.cumsum(np.cumsum(img_orig, 0), 1)
    img_blur = np.zeros((height, width, 3), np.uint8)

    for i in range(0, height):
        i1, i2 = i - (h // 2) - 1, i + (h // 2)
        dh = 0

        if i1 < 0:
            dh += -i1
            i1 = 0
        if i2 >= height:
            dh += i2 - (height - 1)
            i2 = height - 1

        for j in range(0, width):
            j1, j2 = j - (w // 2) - 1, j + (w // 2)
            dw = 0

            if j1 < 0:
                dw += -j1
                j1 = 0
            if j2 >= width:
                dw += j2 - (width - 1)
                j2 = width - 1

            sum = img_sum[i2][j2] + img_sum[i1][j1] - img_sum[i1][j2] - img_sum[i2][j1]
            img_blur[i][j] = sum // ((w - dw) * (h - dh))

    cv2.imwrite(dst_path, img_blur)

def main():
	if __name__ == '__main__':
		if len(argv) == 4:
			argv[3] = int(argv[3])
		box_filter(*argv[1:])