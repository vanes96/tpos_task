import cv2
import os.path
import unittest
from sys import argv
from program import box_filter


class Tests(unittest.TestCase):
    def setUp(self):
        self.n_args = len(argv)
        self.img_orig_name = "lena.jpg"
        self.img_blur_name = "output.jpg"
        if self.n_args > 1:
            self.img_orig_name = argv[1]
        if self.n_args > 2:
            self.img_blur_name = argv[2]
        box_filter(*argv[1:])

    def test_orig_image_existence(self):
        assert os.path.exists(self.img_orig_name)

    def test_blur_image_existence(self):
        assert os.path.exists(self.img_blur_name)

    def test_image_sizes(self):
        img_orig = cv2.imread(self.img_orig_name)
        img_blur = cv2.imread(self.img_blur_name)
        self.assertEqual(img_orig.shape, img_blur.shape)


if __name__ == '__main__':
    unittest.main()