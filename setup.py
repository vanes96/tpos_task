from setuptools import setup, find_packages

setup(
   name='project',
   version='1.0',
   description='box filtration',
   author='Chilikin Ivan',
   author_email='vanesdacha@mail.ru',
   packages=['project'],
   install_requires=['numpy','opencv-python',],
   entry_points={'console_scripts': ['box_filter = project.program:main']},
   zip_safe=False
   #install_requires=['bar', 'greek'], #external packages as dependencies
)
